# SQL Querying - Starter Pack

## How to use

### Prerequisites

You need to have `Docker Engine` ([Debian (and other linux distribs)](https://docs.docker.com/install/linux/docker-ce/debian/), [OSX](https://docs.docker.com/docker-for-mac/install/) or [Windows](https://docs.docker.com/docker-for-windows/install/)) and [Docker Compose](https://docs.docker.com/compose/install/) installed on your computer.

You can also use `make` to launch commands faster but it's not mandatory.

### See list of available commands

`make h` or `make help` or just check the makefile
